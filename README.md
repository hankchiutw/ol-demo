# ol-demo 👋

> Demonstrate the API usage of OpenLayers

## How to run

```sh
yarn install
yarn dev
```

And browse `http://localhost:3000`.

## OpenLayers basics
- Official document: [Basic Concepts](https://openlayers.org/en/latest/doc/tutorials/concepts.html).
- A `Map` contains `View` and layers, e.g. `ImageLayer`, `VectorLayer`, `TileLayer`.
- A `View` handles zoom levels.
- A layer has certain kind of source, e.g. `VectorSource`, `ImageStatic`.
- A source has several `Feature`s.
- A `Feature` is rendered in certain kind of `Geometry`, e.g. `Point`, `Polygon`.
- `Projection` defines how to map the real coordinate to the `Extent`.
- A `Map` can have several `Interaction`s.
  - To deal with the mouse events.
  - Official examples: [Select Features](https://openlayers.org/en/latest/examples/select-features.html), [Vector Layer Hit Detection](https://openlayers.org/en/latest/examples/hitdetect-vector.html)
- WebGL
  - Currently only support `WebGLPointsLayer`.
  - Official examples: [WebGL points layer](https://openlayers.org/en/latest/examples/webgl-points-layer.html), [Rendering points with WebGL](https://openlayers.org/workshop/en/webgl/points.html)

## About this repo
- Initialized using [Vite](https://vitejs.dev/guide/): `yarn create vite ol-demo --template vanilla-ts`
- [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html) enabled. See [.gitlab-ci.yml](.gitlab-ci.yml).

## Author

👤 **hank**

