import './style.css';
import { start as startOL } from './ol';
import { start as startWebGL } from './webgl';
import { start as startOSD } from './osd';
import { start as startHeader } from './header';
import { hitTester } from './hitTester';

hitTester.watch(document.body);
startHeader();
startOL();
startWebGL();
startOSD();
