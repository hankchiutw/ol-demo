import paper from 'paper';

(window as any).paper = paper;

const defaultStyle = {
  strokeColor: new paper.Color('blue'),
  strokeScaling: false,
  strokeWidth: 3,
  // opacity: 1,
  // visible: true,
  fillColor: new paper.Color(0, 0, 0, 0.01),
};

export function paperFactory(viewer) {
  const canvas = document.createElement('canvas');
  canvas.setAttribute(
    'style',
    `
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    pointer-events: none;
  `
  );

  // To have correct canvas size, need to append the canvas before actually creating the paper instance
  viewer.canvas.insertAdjacentElement('afterend', canvas);
  const project = new paper.Project(canvas);
  project.currentStyle = { ...defaultStyle };
  return project;
}
