import paper from 'paper';

export function layerFactory(project) {
  const layer = new paper.Layer({ name: `layer-${Date.now()}` });
  project.addLayer(layer);
  return layer;
}
