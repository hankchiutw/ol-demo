import { get } from '@app/store';

function toItemJSON(segments: any) {
  const item = [
    'Group',
    {
      children: [
        [
          'Path',
          {
            segments: segments || [],
            closed: true,
            fillColor: [0, 0, 0, 0.01],
            strokeColor: [0, 0, 1],
            strokeWidth: 3,
          },
        ],
      ],
    },
  ];

  return item;
}

function rectangle() {
  const dimension = get('dimension');
  const cellDimension = get('cellDimension');
  const x = Math.random() * dimension;
  const y = Math.random() * dimension;

  const sizeX = Math.random() * cellDimension;
  const sizeY = Math.random() * cellDimension;

  const segments = [
    [x, y],
    [x + sizeX, y],
    [x + sizeX, y + sizeY],
    [x, y + sizeY],
  ];
  return JSON.stringify(toItemJSON(segments));
}

function randomItems() {
  const items = [];

  for (let i = 0; i < get('cellCount'); i++) {
    const serializedData = rectangle();

    items.push({
      serializedData,
    });
  }

  return items;
}

export { randomItems };
