import { subscribe } from '@app/store';
import { osdFactory } from './osd';
import { paperFactory, layerFactory } from './paper';
import { initHandlers } from './handlers';
import { randomItems } from './layer-items';

function start() {
  const osd = osdFactory();
  const project = paperFactory(osd);
  const layer = layerFactory(project);

  initHandlers(osd, project);
  refreshLayer(layer);
  subscribe(() => {
    refreshLayer(layer);
  });
}

function refreshLayer(layer) {
  layer.clear();
  randomItems().forEach(({ serializedData }) => {
    layer.importJSON(serializedData);
  });
}

export { start };
