import OSD from 'openseadragon';
import paper from 'paper';
import { hitHandler } from './hit-handler';
import { viewportHandler } from './viewport-handler';

export function initHandlers(osd: OSD, project: paper.Project) {
  viewportHandler(osd, project);
  hitHandler(osd, project);
}
