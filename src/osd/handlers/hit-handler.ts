import OSD from 'openseadragon';
import paper from 'paper';
import { hitTester } from '../../hitTester';

export function hitHandler(osd: OSD, project: paper.Project) {
  const layer = new paper.Layer({ name: `highlight-${Date.now()}` });

  osd.addHandler('canvas-click', ({ position: { x, y } }) => {
    hitTester.start();
    const point = project.view.viewToProject(new paper.Point(x, y));
    const item = hitTest(project, point);
    if (item) {
      layer.removeChildren();

      layer.addChild(
        new paper.Path.Rectangle({
          rectangle: item.strokeBounds,
          strokeColor: new paper.Color('red'),
        })
      );
      hitTester.measure();
    }
  });
}

function hitTest(project: paper.Project, point: paper.Point) {
  const hitResults = project.hitTestAll(point);
  if (!hitResults) {
    return null;
  }
  const items = hitResults
    .map(result => {
      let item = result.item;
      while (!(item.parent.className === 'Layer')) {
        item = item.parent;
      }

      if (!!item.layer.data.preventHitTest) {
        return null;
      }
      return item;
    })
    .filter(item => !!item)
    .filter(item => !item.layer.name.endsWith('-highlight'))
    .sort(
      (a, b) =>
        Math.abs((a as paper.Path).area || Number.MAX_VALUE) -
        Math.abs((b as paper.Path).area || Number.MAX_VALUE)
    );
  return items.length > 0 ? items[0] : null;
}
