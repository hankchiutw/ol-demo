import OSD from 'openseadragon';
import paper from 'paper';

export function viewportHandler(osd: OSD, project: paper.Project) {
  const handler = () => {
    const viewport = getViewport(osd);
    updatePaperView(project, viewport);
  };

  ['open', 'viewport-change', 'rotate', 'flip', 'resize'].forEach(eventName => {
    // add handler after viewer opened otherwise we may get undefined OSD
    osd.addHandler(eventName, handler);
  });
}

function getViewport(osd) {
  const tiledImage = osd.world.getItemAt(0);
  const viewportZoom = osd.viewport.getZoom(true);
  const zoom = tiledImage.viewportToImageZoom(viewportZoom);
  const viewportCenter = tiledImage.viewportToImageCoordinates(osd.viewport.getCenter(true));

  return {
    center: viewportCenter,
    zoom,
    angle: osd.viewport.getRotation(),
  };
}

function updatePaperView(project, { center, zoom, angle }) {
  project.view.center = new paper.Point(center);
  project.view.zoom = zoom;
  project.view.rotation = angle;
  project.view.update();

  //project.view.element.style.transform = `scaleX(1)`;
}
