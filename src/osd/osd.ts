import OSD from 'openseadragon';
import { get } from '@app/store';

const defaultOptions = {
  showNavigationControl: false,
  showNavigator: false,
  navigatorRotate: false,
  navigatorPosition: 'ABSOLUTE',
  navigatorAutoFade: false,
  navigatorBorderColor: 'rgba(0, 0, 0, 0)',
  navigatorDisplayRegionColor: null,
  loadTilesWithAjax: true,
  maxZoomPixelRatio: 3,
  minPixelRatio: 0.5,
  minZoomImageRatio: 0.9,
  imageLoaderLimit: 0,
  maxImageCacheCount: 1000,
  minZoomLevel: null,
  maxZoomLevel: null,
  smoothTileEdgesMinZoom: 1.1,
  timeout: 1000000,
  preserveImageSizeOnResize: true,
  immediateRender: false,
  gestureSettingsMouse: {
    clickToZoom: false,
  },
};

export function osdFactory() {
  const osd = OSD({
    ...defaultOptions,
    id: 'osd',
    tileSources: [
      {
        type: 'image',
        url: get('imageUrl'),
        crossOriginPolicy: 'Anonymous',
        ajaxWithCredentials: false,
      },
    ],
  });

  osd.viewport.centerSpringX.animationTime = 0;
  osd.viewport.centerSpringY.animationTime = 0;

  //osd.viewport.zoomSpring.animationTime = 0;
  (window as any).osd = osd;
  return osd;
}
