type Listener = (duration: number) => void;

const listeners: Listener[] = [];

let startTime: number;
let endTime: number;

function watch(element: HTMLElement) {
  element.addEventListener(
    'mousedown',
    () => {
      startTime = Date.now();
    },
    true
  );
}

function measure() {
  endTime = Date.now();

  listeners.forEach(callback => {
    callback(endTime - startTime);
  });
}

function start() {
  startTime = Date.now();
}

function subscribe(callback: Listener) {
  listeners.push(callback);
}

export const hitTester = {
  watch,
  start,
  measure,
  subscribe,
};
