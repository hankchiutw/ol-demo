import { get, set } from '@app/store';
import { hitTester } from '../hitTester';

function start() {
  const rootElm = document.getElementById('header');
  const inputElm = rootElm.querySelector('input');
  const refreshElm = rootElm.querySelector('button');

  inputElm.value = get('cellCount');

  function updateCellCount() {
    set('cellCount', parseInt(inputElm.value));
  }

  refreshElm.addEventListener('click', updateCellCount);
  inputElm.addEventListener('keypress', ({ code }) => {
    code === 'Enter' && updateCellCount();
  });

  hitTester.subscribe((duration: number) => {
    console.log('xxx: hitTest:', duration);
  });
}

export { start };
