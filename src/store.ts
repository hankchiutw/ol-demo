type Listener = (key: string, value: any) => void;

const listeners: Listener[] = [];

const store: Record<string, any> = {
  dimension: 1000,
  cellDimension: 10,
  cellCount: 1000,
  get imageUrl() {
    return `https://picsum.photos/${this.dimension}`;
  },
};

function get(key: string) {
  return store[key];
}

function set(key: string, value: any) {
  store[key] = value;
  listeners.forEach(callback => {
    callback(key, value);
  });
}

function subscribe(callback: Listener) {
  listeners.push(callback);
}

export { get, set, subscribe };
