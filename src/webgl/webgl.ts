import Map from 'ol/Map';
import View from 'ol/View';
import Feature from 'ol/Feature';
import {
  WebGLPoints as WebGLPointsLayer,
  Image as ImageLayer,
  Vector as VectorLayer,
} from 'ol/layer';
import { Vector as VectorSource, ImageStatic } from 'ol/source';
import Projection from 'ol/proj/Projection';
import { Extent, getCenter } from 'ol/extent';
import { Style, Stroke } from 'ol/style';
import { SymbolType } from 'ol/style/LiteralStyle';
import { Select } from 'ol/interaction';
import { Polygon, Point } from 'ol/geom';
import { randomFeatures } from './features';
import { get, subscribe } from '@app/store';
import { hitTester } from '../hitTester';

const projection = new Projection({
  code: 'static-image',
  units: 'pixels',
});

function initMap() {
  const extent: Extent = [0, 0, get('dimension'), get('dimension')];
  projection.setExtent(extent);

  return new Map({
    target: 'webgl',
    layers: [
      new ImageLayer({
        source: new ImageStatic({
          url: get('imageUrl'),
          crossOrigin: 'Anonymous',
          projection,
          imageExtent: extent,
        }),
      }),
      // new TileLayer({
      //   source: new XYZ({
      //     url: 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
      //   })
      // }),
    ],
    view: new View({
      projection,
      center: getCenter(extent),
      zoom: 1,
    }),
  });
}

function initAnnotationLayer() {
  return new WebGLPointsLayer({
    source: new VectorSource(),
    style: {
      symbol: {
        symbolType: SymbolType.SQUARE,
        color: 'blue',
        // Note: equals to size * 2^(zoom - 2)
        size: ['*', ['*', ['get', 'size'], ['^', 2, ['zoom']]], 0.25],
        opacity: 0.7,
      },
    },
  });
}

function refreshLayer(layer: WebGLPointsLayer) {
  const source = layer.getSource() as VectorSource;
  source.clear();
  source.addFeatures(randomFeatures());
  // TODO: update extent
}

function addSelectInteraction(map: Map, layer: WebGLPointsLayer) {
  const selectInteraction = new Select({
    layers: [layer],
  });

  map.addInteraction(selectInteraction);

  const highlightOverlay = new VectorLayer({
    source: new VectorSource(),
    map: map,
    style: new Style({
      stroke: new Stroke({
        color: 'red',
        width: 3,
      }),
    }),
  });

  let selectedFeature: Feature<Point>;
  selectInteraction.on('select', e => {
    if (e.selected.length === 0) {
      return;
    }
    if (selectedFeature === e.selected[0]) {
      return;
    }
    selectedFeature = e.selected[0] as Feature<Point>;
    highlightOverlay.getSource().clear();

    const [x, y] = selectedFeature.getGeometry().getCoordinates();
    const { size } = selectedFeature.getProperties();
    highlightOverlay.getSource().addFeature(
      new Feature(
        new Polygon([
          [
            [x - size / 2, y - size / 2],
            [x + size / 2, y - size / 2],
            [x + size / 2, y + size / 2],
            [x - size / 2, y + size / 2],
          ],
        ])
      )
    );
    hitTester.measure();
  });
}

function start() {
  const map = initMap();
  const annotationLayer = initAnnotationLayer();
  map.addLayer(annotationLayer);

  addSelectInteraction(map, annotationLayer);

  refreshLayer(annotationLayer);
  subscribe(() => {
    refreshLayer(annotationLayer);
  });
}

export { start };
