import Feature from 'ol/Feature';
import { Point } from 'ol/geom';
import { Coordinate } from 'ol/coordinate';
import { get } from '@app/store';

function point(): Coordinate {
  const dimension = get('dimension');

  const x = Math.random() * dimension;
  const y = Math.random() * dimension;

  return [x, y];
}

function randomFeatures(): Feature[] {
  return Array(get('cellCount'))
    .fill(true)
    .map(() => {
      const cellDimension = get('cellDimension');
      const size = Math.random() * cellDimension;

      return new Feature({
        size,
        geometry: new Point(point()),
      });
    });
}

export { randomFeatures };
