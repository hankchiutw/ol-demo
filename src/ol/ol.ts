import Map from 'ol/Map';
import View from 'ol/View';
import { Vector as VectorLayer, Image as ImageLayer } from 'ol/layer';
import { Vector as VectorSource, ImageStatic } from 'ol/source';
import Projection from 'ol/proj/Projection';
import { Extent, getCenter } from 'ol/extent';
import { Style, Fill, Stroke } from 'ol/style';
import { Select } from 'ol/interaction';
import { randomFeatures } from './features';
import { get, subscribe } from '@app/store';
import { hitTester } from '../hitTester';

const projection = new Projection({
  code: 'static-image',
  units: 'pixels',
});

function initMap() {
  const extent: Extent = [0, 0, get('dimension'), get('dimension')];
  projection.setExtent(extent);

  return new Map({
    target: 'ol',
    layers: [
      new ImageLayer({
        source: new ImageStatic({
          url: get('imageUrl'),
          crossOrigin: 'Anonymous',
          projection,
          imageExtent: extent,
        }),
      }),
      // new TileLayer({
      //   source: new XYZ({
      //     url: 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
      //   })
      // }),
    ],
    view: new View({
      projection,
      center: getCenter(extent),
      zoom: 1,
    }),
  });
}

function initAnnotationLayer() {
  return new VectorLayer({
    source: new VectorSource(),
    style: new Style({
      stroke: new Stroke({
        color: 'blue',
        width: 2,
      }),
      fill: new Fill({
        color: 'rgba(0, 0, 0, 0.01)',
      }),
    }),
  });
}

function refreshLayer(layer: VectorLayer) {
  layer.getSource().clear();
  layer.getSource().addFeatures(randomFeatures());
  // TODO: update extent
}

function addSelectInteraction(map: Map, layer: VectorLayer) {
  const selectInteraction = new Select({
    style: new Style({
      stroke: new Stroke({
        color: 'red',
        width: 3,
      }),
    }),
    layers: [layer],
  });

  map.addInteraction(selectInteraction);
  selectInteraction.on('select', () => {
    hitTester.measure();
  });
}

function start() {
  const map = initMap();
  const annotationLayer = initAnnotationLayer();
  map.addLayer(annotationLayer);

  addSelectInteraction(map, annotationLayer);

  refreshLayer(annotationLayer);
  subscribe(() => {
    refreshLayer(annotationLayer);
  });
}

export { start };
