import Feature from 'ol/Feature';
import { Polygon } from 'ol/geom';
import { Coordinate } from 'ol/coordinate';
import { get } from '@app/store';

function rectangle(): Coordinate[] {
  const dimension = get('dimension');
  const cellDimension = get('cellDimension');

  const x = Math.random() * dimension;
  const y = Math.random() * dimension;

  const sizeX = Math.random() * cellDimension;
  const sizeY = Math.random() * cellDimension;

  const segments: Coordinate[] = [
    [x, y],
    [x + sizeX, y],
    [x + sizeX, y + sizeY],
    [x, y + sizeY],
  ];

  return segments;
}

function randomFeatures(): Feature[] {
  return Array(get('cellCount'))
    .fill(true)
    .map(
      () =>
        new Feature({
          geometry: new Polygon([rectangle()]),
        })
    );
}

export { randomFeatures };
